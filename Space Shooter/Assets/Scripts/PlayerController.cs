﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable] //allows serialization in engine

public class Constraints
{
	public float xMin, xMax, zMin, zMax;
}

public class PlayerController : MonoBehaviour 
{
	public int sensitivity = 1;
	public Constraints playerConstraints;
	public float tilt = 0.0f;
	public float shotCooldown;
	private float nextFire = 0.0f;
	public GameObject bolt;
	public Transform boltTransform;

	void Update()
	{
		if (Input.GetButton ("Fire1") && Time.time > nextFire) 
		{
			nextFire = Time.time + shotCooldown;
			Instantiate (bolt, boltTransform.position, boltTransform.rotation);
			AudioSource playerAudio = GetComponent<AudioSource> ();
			playerAudio.Play ();
		}
	}
	void FixedUpdate () 
	{
		//getting inputs
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");
		//getting rigid body component (i.e.) the space ship
		Rigidbody player = GetComponent <Rigidbody>();
		//setting the input to velocity
		Vector3 playerVelocity = new Vector3 (moveHorizontal, 0, moveVertical);
		//adding sensitivity to control ship speed
		player.velocity = playerVelocity * sensitivity;
		//clamping x and z coordinates
		player.position = new Vector3 (
		Mathf.Clamp (player.position.x, playerConstraints.xMin, playerConstraints.xMax),
		0.0f,
		Mathf.Clamp (player.position.z, playerConstraints.zMin, playerConstraints.zMax)
		);
		player.rotation = Quaternion.Euler (0.0f, 0.0f, player.velocity.x*-tilt);

	}
}

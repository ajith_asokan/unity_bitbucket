﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour {
	public float boltSpeed;
	// Use this for initialization
	void Start () 
	{
		Rigidbody bolt = GetComponent<Rigidbody> ();
		bolt.velocity = transform.forward * boltSpeed;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionObjectDuration : MonoBehaviour {
	public float explosionObjectLife;
	void Start()
	{
		Destroy (gameObject, explosionObjectLife);
	}

}

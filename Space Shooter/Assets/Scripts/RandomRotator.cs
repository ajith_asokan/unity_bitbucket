﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomRotator : MonoBehaviour {

	private float asteroidTumble;
	void Start()
	{
		float asteroidTumble = Random.Range (1.0f, 5.0f);
		Rigidbody asteroid = GetComponent<Rigidbody> ();
		asteroid.angularVelocity = Random.insideUnitSphere * asteroidTumble;
	}

}

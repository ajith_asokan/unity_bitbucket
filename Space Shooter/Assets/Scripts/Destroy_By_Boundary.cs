﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroy_By_Boundary : MonoBehaviour 
{
	void OnTriggerExit(Collider other)
	{
		// Destroy bolts that leave the trigger
		if(other.tag == "bolt" || other.tag == "asteroid")
			Destroy(other.gameObject);
	}

}

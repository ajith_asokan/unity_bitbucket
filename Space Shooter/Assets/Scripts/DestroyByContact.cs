﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContact : MonoBehaviour 
{
	public GameObject asteroidExplosion;
	public GameObject playerExplosion;
	private GameController gameController;
	public int points;
	void Start()
	{
		GameObject gameControllerObject = GameObject.FindGameObjectWithTag("GameController");
		if (gameControllerObject != null) 
		{
			gameController = gameControllerObject.GetComponent<GameController> ();
		}
		if (gameControllerObject == null) 
		{
			Debug.Log ("Cannot Find Game Controller Object");
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.tag != "boundary") 
		{
			Destroy (other.gameObject);
			Destroy (gameObject);
			Instantiate (asteroidExplosion, gameObject.transform.position , gameObject.transform.rotation);
			if (other.tag == "Player") 
			{
				Instantiate (playerExplosion, other.transform.position, other.transform.rotation);
				gameController.GameIsOver ();
			}
			if (other.tag == "bolt") 
			{
				gameController.AddScore (points);
			}
		}
	}
}

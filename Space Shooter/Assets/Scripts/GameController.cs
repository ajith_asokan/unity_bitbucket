﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]

public class WaitTimes {public float startWait, spawnWait;};

public class GameController : MonoBehaviour 
{
	public GameObject hazard;
	public WaitTimes waitTimes;
	public int hazardCount;
	public float waveWait;
	public GUIText scoreGUI;
	public GUIText gameOverGUI;
	public GUIText restartGUI;
	bool gameOver;
	bool restart;
	private int score = 0;
	// Use this for initialization
	void Start () 
	{
		UpdateScore ();
		StartCoroutine (SpawnWaveCR ());
		gameOverGUI.text = "";
		restartGUI.text = "";
		gameOver = false;
		restart = false;
	}

	IEnumerator SpawnWaveCR()
	{
		yield return new WaitForSeconds (waitTimes.startWait);  
		while (true) 
		{
			for (int i = 0; i <= hazardCount; i++) 
			{
				SpawnHazard ();
				yield return new WaitForSeconds (waitTimes.spawnWait);  
			}
			yield return new WaitForSeconds (waveWait);
			if (gameOver) 
			{
				break;
			}
		}
	}

	public void GameIsOver()
	{
		gameOver = true;
		gameOverGUI.text = "Game Over";
		restartGUI.text = "(Press 'R' to restart the game)";
	}

	void Update()
	{
		if (Input.GetKey (KeyCode.R)) 
		{
			restart = true;;
		}
		else if(Input.GetKey("escape"))
		{
			Debug.Log ("'Esc' Pressed!");
			Application.Quit();
		}
		if (restart == true) 
		{
			Debug.Log ("'R' Pressed!");
			Application.LoadLevel (Application.loadedLevel);
		}
	}

	void SpawnHazard ()
	{
		Vector3 hazardPosition = new Vector3 (Random.Range (-6.0f, 6.0f), 0.0f, 16);
		Quaternion hazardRotation = Quaternion.identity;
		Instantiate (hazard, hazardPosition, hazardRotation);
	}
		
	public void AddScore(int newScore)
	{
		score += newScore;
		UpdateScore ();
	}

	void UpdateScore()
	{
		scoreGUI.text = "Score: " + score;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using UnityEditor;

public class DebitPanelController : MonoBehaviour 
{
	public Button cancel;
	public Button proceed;
	public Dropdown month,year;
	public InputField pin, cvv,nameOnDC;
	float height, width;
	Animator panelAnim;
	void Start () 
	{
		panelAnim = GetComponent<Animator> ();
		//GameObject ccButton = GameObject.FindWithTag ("credit card button");
		proceed.interactable = false;
		cancel.onClick.AddListener (OnCancel);
		proceed.onClick.AddListener (OnProceed);
	}
	void Update()
	{
		string sPin = pin.text.ToString ();
		//string sCvv = cvv.text.ToString ();
		string sName = nameOnDC.text.ToString ();
		if (sPin.Length == 16 && sName.Length >= 1)
			proceed.interactable = true;
	}
	void OnCancel()
	{
		panelAnim.SetTrigger ("tExit");
		/*nameOnCC.text = "";
		pin.text = "";
		month.value = 0;
		year.value = 0;
		gameObject.SetActive (false);*/
		//int fade = gameObject.GetComponent<Animation> ().GetClipCount();
		//Debug.Log ("clip =" + fade);

		Destroy (gameObject,1.0f);
		GameObject dcButton = GameObject.FindWithTag ("debit card button");
		if (dcButton == null)
			Debug.Log ("cc Button not activated");
		dcButton.GetComponent<Button>().interactable = true;
	}
	void OnProceed()
	{
		#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
		#elif UNITY_WEBPLAYER
		Application.OpenURL(webplayerQuitURL);
		#else
		Application.Quit();
		#endif
	}


}

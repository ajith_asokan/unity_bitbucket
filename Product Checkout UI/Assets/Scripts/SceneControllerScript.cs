﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneControllerScript : MonoBehaviour {
	public GameObject creditCardPanel,debitCardPanel;
	public Button creditCardButton,debitCardButton;
	//bool ccPanelActive;
	public GameObject canvas;


	// Use this for initialization
	void Start () {
		creditCardButton.onClick.AddListener(OnCreditCardButtonClick);
		debitCardButton.onClick.AddListener(OnDebitCardButtonClick);
	}
	void OnCreditCardButtonClick()
	{
		//GameObject cCPanel = GameObject.FindGameObjectWithTag ("credit card panel");
		GameObject cCPanel = Instantiate (creditCardPanel);
		cCPanel.transform.SetParent (canvas.transform, false);
		//ccPanelActive = cCPanel.activeSelf;
		//Debug.Log ("Panel is" + cCPanel.activeInHierarchy + " in heirarchy");
		//Debug.Log ("Panel is " + active);
		creditCardButton.interactable = false;
	}
	void OnDebitCardButtonClick()
	{
		//GameObject cCPanel = GameObject.FindGameObjectWithTag ("credit card panel");
		GameObject dCPanel = Instantiate (debitCardPanel);
		dCPanel.transform.SetParent (canvas.transform, false);
		//ccPanelActive = cCPanel.activeSelf;
		//Debug.Log ("Panel is" + cCPanel.activeInHierarchy + " in heirarchy");
		//Debug.Log ("Panel is " + active);
		debitCardButton.interactable = false;
	}
	// Update is called once per frame
	void Update () 
	{
		//if(ccPanelActive)
			//creditCardButton.interactable = true;
	}
}
